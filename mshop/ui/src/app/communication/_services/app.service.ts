import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment as env} from '../../../environments/environment';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class AppService <T> {

  protected _baseURL = "http://localhost:8080";

  protected constructor(protected _http: HttpClient) {
    //this._baseURL = env.apiUrl + baseURI;
  }

  addTToken(url: string) {
    //AuthService.addSessionStorage(url, functionId);
  }

  get(id: any, maTk?: string): Observable<T> {
    if (maTk && maTk !== null) {
      this.addTToken(`${this._baseURL}/${maTk}/${id}`);
      return this._http.get<T>(`${this._baseURL}/${maTk}/${id}`);
    } else {
      this.addTToken(`${this._baseURL}/${id}`);
      return this._http.get<T>(`${this._baseURL}/${id}`);
    }
  }

  create(model: T, maTk?: string): Observable<T> {
    if (maTk && maTk !== null) {
      this.addTToken(`${this._baseURL}/${maTk}`);
      return this._http.post<T>(`${this._baseURL}/${maTk}`, model);
    } else {
      this.addTToken(`${this._baseURL}`);
      return this._http.post<T>(`${this._baseURL}`, model);
    }
  }

  save(model: T, maTk?: string): Observable<T> {
    if (maTk && maTk !== null) {
      this.addTToken(`${this._baseURL}/${maTk}`);
      return this._http.post<T>(`${this._baseURL}/${maTk}`, model);
    } else {
      this.addTToken(`${this._baseURL}`);
      return this._http.post<T>(`${this._baseURL}`, model);
    }
  }

  update(id: any, model: T, maTk?: string): Observable<T> {
    if (maTk && maTk !== null) {
      this.addTToken(`${this._baseURL}/${maTk}/${id}`);
      return this._http.put<T>(`${this._baseURL}/${maTk}/${id}`, model);
    } else {
      this.addTToken(`${this._baseURL}/${id}`);
      return this._http.put<T>(`${this._baseURL}/${id}`, model);
    }
  }

  delete(id: any, maTk?: string): Observable<any> {
    if (maTk && maTk !== null) {
      this.addTToken(`${this._baseURL}/${maTk}/${id}`);
      return this._http.delete(`${this._baseURL}/${maTk}/${id}`);
    } else {
      this.addTToken(`${this._baseURL}/${id}`);
      return this._http.delete(`${this._baseURL}/${id}`);
    }
  }
}
