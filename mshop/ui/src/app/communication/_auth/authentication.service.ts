import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'app/models/User';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    _user :User;
    BASE_URL = "http://localhost:8080";
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {  
         var payload = new FormData();
         payload.append("username", username);
         payload.append('password', password);
        return this.http.post<string>(this.BASE_URL+`/login`, payload, {responseType: 'json'}).pipe(map(res => {
            // login successful if there's a jwt token in the response
            debugger;
            if (res && res) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                this._user = new User();
                this._user.username = 'admin';
                this._user.token = res;
                localStorage.setItem('currentUser', JSON.stringify(this._user));
                this.currentUserSubject.next(this._user);
            }
            return res;
        }));
    }

    logout() {
        // remove user from local srage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}