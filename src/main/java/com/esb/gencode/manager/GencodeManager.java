package com.esb.gencode.manager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import org.reflections.Reflections;
import javax.persistence.*;

import org.apache.commons.io.IOUtils;

import com.esb.gencode.model.entity.Booking;

public class GencodeManager {
	// Khai bao cac contructor
	static String FILE_TYPE_CONTROLLER = "CONTROLLER";
	static String FILE_TYPE_DAO = "DAO";
	static String FILE_TYPE_SERVICE = "SERVICE";
	
	public static void main(String[] args) throws Exception {
		// Tim kiem danh sach class 
		File folder = new File("/Users/tienmanh/Documents/Data/Gencode/Template/Model/");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
		  if (listOfFiles[i].isFile()) {
		    String fileName = listOfFiles[i].getName().split("\\.")[0];
		    Class<?> clsGencode = Class.forName("com.esb.gencode.model.entity."+fileName);
			 Table tbl = (Table) clsGencode.getAnnotation(Table.class);
			 String rootTableName = tbl.name();
			 String entityTableName = clsGencode.getName();
			 if(entityTableName.indexOf(".") > -1) {
				 String[] entityTableNameArray = entityTableName.split("\\.");
				 entityTableName = entityTableNameArray[entityTableNameArray.length-1];
			 }
			 String entityTableNameObject = entityTableName.substring(0,1).toLowerCase()+entityTableName.substring(1);
			genCode(entityTableName, entityTableNameObject, rootTableName, clsGencode, FILE_TYPE_DAO);
			genCode(entityTableName, entityTableNameObject, rootTableName, clsGencode, FILE_TYPE_CONTROLLER);
			genCode(entityTableName, entityTableNameObject, rootTableName, clsGencode, FILE_TYPE_SERVICE);
		  } else if (listOfFiles[i].isDirectory()) {
		    System.out.println("Directory " + listOfFiles[i].getName());
		  }
		}
	}
	
	// Thuc hien generic file dao 
		public static void genCode(String entityTableName, String entityTableNameObject, String rootTableName,Class clsGencode, String fileGenCodeType) {
			InputStream inStream = null;
			OutputStream outStream = null;
			try {
				String fileTypeName = "";
				if(fileGenCodeType.equals(FILE_TYPE_CONTROLLER)) fileTypeName = "Controller";
				else if(fileGenCodeType.equals(FILE_TYPE_DAO)) fileTypeName = "Dao";
				else if(fileGenCodeType.equals(FILE_TYPE_SERVICE)) fileTypeName = "Service";
				// Bo sung them  so contructor 
				String GC_TABLE_ENTITY_LOWER = entityTableName.toLowerCase();
				String GC_TABLE_ENTITY_UPPER = entityTableName.toUpperCase();
				// Coppy file mau ve thu muc 
				File fileIn =new File("/Users/tienmanh/Documents/Data/Gencode/Template/Dao/Template"+fileTypeName+".java");
				File fileOut =new File("/Users/tienmanh/Documents/Data/Gencode/Template/Output/"+fileTypeName+"/"+entityTableName+fileTypeName+".java");
				if(fileOut.createNewFile()){
		            System.out.println("file.txt File Created in Project root directory");
		        }else System.out.println("File file.txt already exists in the project root directory");
				inStream = new FileInputStream(fileIn);
	    	    outStream = new FileOutputStream(fileOut);
	    	    byte[] buffer = new byte[1024];
	    	    int length;
	    	    //copy the file content in bytes 
	    	    while ((length = inStream.read(buffer)) > 0){
	    	    	outStream.write(buffer, 0, length);
	    	    }
	    	    // Thuc hien replace code 
	    	    Path path = Paths.get(fileOut.getPath());
	    	    Charset charset = StandardCharsets.UTF_8;
	    	    String content = new String(Files.readAllBytes(path), charset);
	    	    content = content.replaceAll("GC_TABLE_ENTITY_LOWER", GC_TABLE_ENTITY_LOWER);
	    	    content = content.replaceAll("GC_TABLE_ENTITY_UPPER", GC_TABLE_ENTITY_UPPER);
	    	    content = content.replaceAll("GC_TABLE_CLASS_DAO", entityTableName+"Dao");
	    	    content = content.replaceAll("GC_TABLE_ROOT", rootTableName);
	    	    content = content.replaceAll("GC_TABLE_ID", entityTableName+"Id");
	    	    content = content.replaceAll("GC_TABLE_ITEM", entityTableName.toLowerCase()+"Item");
	    	    content = content.replaceAll("GC_TABLE_ENTITY_OBJECT", entityTableNameObject);
	    	    content = content.replaceAll("GC_TABLE_ENTITY", entityTableName);
	    	    content = content.replaceAll("GC_LST_TABLE_ENTITY","lst"+entityTableName);
	    	    // Thuc hien Generic tham so GC_LST_PROPERTY_PARAM, GC_LST_PROPERTY_NAME_SQL
	    	    String GC_LST_PROPERTY_PARAM = "";
	    	    String GC_LST_PROPERTY_NAME_SQL = "";
	    	    String GC_SET_OBJECT_PROPERTY_NAME_SQL="";
	    	    String GC_QUERY_GET_BY_CONDITION_SQL = "select ";
	    	    String GC_VARIABLE_RESULT_SET = "";
	    	    String GC_ENTITY_SET_PROPERTY = "";
	    	    int indexSetObject = 0;
				for (Field field : clsGencode.getDeclaredFields()) {
					// doc type va doc nam Class type
					String type = field.getType().getName();
					String name = field.getName();
					// Xoa bo cac text co dau cham trong type
					if(type.indexOf(".") > -1) {
						String[] typeArray = type.split("\\.");
						type = typeArray[typeArray.length-1];
					}
					GC_LST_PROPERTY_PARAM += type + " " + name+",";
					System.out.println("type : " + type + ". name: " + name+",");
					// Doc conlum name
					Column col = field.getAnnotation(Column.class);
					if (col != null) {
						indexSetObject++;
						System.out.println("Comlum name : " + col.name());
						GC_LST_PROPERTY_NAME_SQL+="sql +=\" and "+col.name()+"= ? \"; \n";
						GC_SET_OBJECT_PROPERTY_NAME_SQL+=" ps.setObject("+indexSetObject+", "+field.getName()+"); \n";
						GC_QUERY_GET_BY_CONDITION_SQL+=col.name()+",";
						GC_VARIABLE_RESULT_SET+=type + " v_"+col.name().toLowerCase() + " = rs.getObject(\""+col.name()+"\"); \n";
						GC_ENTITY_SET_PROPERTY+=entityTableNameObject+".set"+type.substring(0,1).toUpperCase()+type.substring(1)+"("+"v_"+col.name().toLowerCase()+"); \n";
					}
				}
	    	    GC_LST_PROPERTY_PARAM = GC_LST_PROPERTY_PARAM.substring(0, GC_LST_PROPERTY_PARAM.length() -1);
	    	    GC_LST_PROPERTY_NAME_SQL = GC_LST_PROPERTY_NAME_SQL.substring(0, GC_LST_PROPERTY_NAME_SQL.length() -1);
	    	    GC_QUERY_GET_BY_CONDITION_SQL= GC_QUERY_GET_BY_CONDITION_SQL.substring(0, GC_QUERY_GET_BY_CONDITION_SQL.length() -1);
	    	    GC_QUERY_GET_BY_CONDITION_SQL+=" from "+ rootTableName +" where 1=1\""+"; \n";
	    	    GC_QUERY_GET_BY_CONDITION_SQL+=GC_LST_PROPERTY_NAME_SQL;
	    	    content = content.replaceAll("GC_LST_PROPERTY_PARAM", GC_LST_PROPERTY_PARAM);
	    	    content = content.replaceAll("GC_LST_PROPERTY_NAME_SQL", GC_LST_PROPERTY_NAME_SQL);
	    	    content = content.replaceAll("GC_SET_OBJECT_PROPERTY_NAME_SQL", GC_SET_OBJECT_PROPERTY_NAME_SQL);
	    	    content = content.replaceAll("GC_QUERY_GET_BY_CONDITION_SQL", GC_QUERY_GET_BY_CONDITION_SQL);
	    	    content = content.replaceAll("GC_VARIABLE_RESULT_SET", GC_VARIABLE_RESULT_SET);
	    	    content = content.replaceAll("GC_ENTITY_SET_PROPERTY", GC_ENTITY_SET_PROPERTY);
	    	    Files.write(path, content.getBytes(charset));
	    	    inStream.close();
	    	    outStream.close();
	    	    System.out.println("File is copied successful!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	
	public static final String NESTED_PROPERTY_SEPARATOR = ".";
	  
	  public static Field getPropertyField(Class<?> beanClass,String property) throws NoSuchFieldException {  
	    if (beanClass == null)
	      throw new IllegalArgumentException("beanClass cannot be null");
	    
	    Field field = null;
	    try {
	      field = beanClass.getDeclaredField(property);
	    } catch (NoSuchFieldException e) {
	      if (beanClass.getSuperclass() == null)
	        throw e;
	      // look for the field in the superClass
	      field = getPropertyField(beanClass.getSuperclass(),property);
	    }
	    return field;
	  }
	  
	  public static Field getField(Class<?> beanClass,String propertyPath) throws NoSuchFieldException {    
	     if (beanClass == null)
	      throw new IllegalArgumentException("beanClass cannot be null");
	    
	    if (propertyPath.indexOf("[") != -1)
	      propertyPath = propertyPath.substring(0,propertyPath.indexOf("["));
	    
	    // if the property path is simple then look for it directly on the class.
	    if (propertyPath.indexOf(NESTED_PROPERTY_SEPARATOR) == -1) {
	      // look if the field is declared in this class.
	      return getPropertyField(beanClass,propertyPath);
	    } else {
	      // if the property is a compound one then split it and look for the first field.
	      // and recursively locate fields of fields.
	      String propertyName = propertyPath.substring(0,propertyPath.indexOf(NESTED_PROPERTY_SEPARATOR));
	      Field field = getField(beanClass,propertyName);

	      // try to locate sub-properties
	      return getField(getTargetType(field),propertyPath.substring(propertyPath.indexOf(NESTED_PROPERTY_SEPARATOR)+1));
	    }
	  }
	  
	  public static Class<?> getTargetType(Field field) {
	    //  Generic type, case when we have a Collection of ?
	    if (field.getGenericType() instanceof ParameterizedType) {
	      ParameterizedType type = (ParameterizedType)field.getGenericType();
	      if (type.getActualTypeArguments().length == 1 && type.getActualTypeArguments()[0] instanceof Class)
	      return (Class<?>)type.getActualTypeArguments()[0];
	    }
	    
	    return field.getType();
	  }
	  
	  public static Class<?> getPropertyClass(Class<?> beanClass,String propertyPath) {
	    try {
	      Field field = getField(beanClass, propertyPath);
	      return(getTargetType(field));
	    } catch (NoSuchFieldException e) {
	      throw new IllegalArgumentException(propertyPath+" is not a property of "+beanClass.getName());
	    }
	  }
	  
	  public static boolean isFieldDeclared(Class<?> beanClass,String propertyPath) {
	    try {
	      return getField(beanClass, propertyPath) != null;
	    } catch(NoSuchFieldException e) {
	      return false;
	    }
	  }
	  
	/*
	 * public static Object getPropertyValue(Object bean,String propertyPath) throws
	 * NoSuchFieldException { if (bean == null) throw new
	 * IllegalArgumentException("bean cannot be null"); Field field =
	 * ReflectionUtils.getField(bean.getClass(), propertyPath);
	 * field.setAccessible(true); try { return(field.get(bean)); } catch
	 * (IllegalAccessException e) { return(null); } }
	 */
}
