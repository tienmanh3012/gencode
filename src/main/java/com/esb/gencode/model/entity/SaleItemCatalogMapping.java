package com.esb.gencode.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="s_item_catalog_mapping")
public class SaleItemCatalogMapping {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name="ID")
    private String id;
	@Column(name="MERCHANT_ID")
	private String merchantId;
	@Column(name="CATALOG_ID")
	private String catalogId;
	@Column(name="ITEM_ID")
	private String itemId;
	@Column(name="TYPE")
	private int type;
	@Column(name="ORDINAL")
	private int ordinal;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
	
}
