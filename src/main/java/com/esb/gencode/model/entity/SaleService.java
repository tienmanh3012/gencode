package com.esb.gencode.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="s_service")
public class SaleService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name="ID")
    private String id;
	@Column(name = "MERCHANT_ID")
    private String merchantId;
	@Column(name = "AVATAR")
    private String avatar;
	private String avatarImageUrl;
	private String avatarImageThumbnailUrl;
	@Column(name = "SERVICE_CODE")
    private String serviceCode;
	@Column(name = "SERVICE_NAME")
    private String serviceName;
	@Column(name = "DURATION")
    private int duration;
	@Column(name = "PRICE")
    private int price;
	@Column(name = "PRICE_TO")
    private int priceTo;
	@Column(name = "PRICE_TYPE")
    private int priceType;
	@Column(name = "DESCRIPTION")
    private String description;
	@Column(name = "BOOKING_ONLINE")
    private int bookingOnline;
	@Column(name = "CATALOG_ID")
    private String catalogId;
	@Column(name = "STATUS")
    private int status;
	
	String bookingDetailId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getPriceTo() {
		return priceTo;
	}
	public void setPriceTo(int priceTo) {
		this.priceTo = priceTo;
	}
	public int getPriceType() {
		return priceType;
	}
	public void setPriceType(int priceType) {
		this.priceType = priceType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getBookingOnline() {
		return bookingOnline;
	}
	public void setBookingOnline(int bookingOnline) {
		this.bookingOnline = bookingOnline;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getBookingDetailId() {
		return bookingDetailId;
	}
	public void setBookingDetailId(String bookingDetailId) {
		this.bookingDetailId = bookingDetailId;
	}
}
