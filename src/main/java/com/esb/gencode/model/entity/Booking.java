package com.esb.gencode.model.entity;

import java.io.Serializable;
/*import com.ati.core.annotation.Column;
import com.ati.core.annotation.Id;
import com.ati.core.annotation.Table;*/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "s_booking")
public class Booking implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	private String id;

	@Column(name = "CUSTOMER_ID")
	private String customerId;

	@Column(name = "CUSTOMER_NAME")
	private String customerName;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "FROM_SOURCE")
	private Integer fromSource;

	@Column(name = "BOOKING_TIME")
	private int bookingTime;

	@Column(name = "TOTAL_AMOUNT")
	private int totalAmount;

	@Column(name = "AMOUNT")
	private int amount;

	@Column(name = "MERCHANT_ID")
	private String merchantId;

	@Column(name = "BOOKING_CODE")
	private String bookingCode;

	@Column(name = "NOTE")
	private String note;

	@Column(name = "STATUS")
	private int status;

	@Column(name = "CREATED_TIME")
	private int createdTime;
	
	private String orderId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getFromSource() {
		return fromSource;
	}

	public void setFromSource(Integer fromSource) {
		this.fromSource = fromSource;
	}

	public int getBookingTime() {
		return bookingTime;
	}

	public void setBookingTime(int bookingTime) {
		this.bookingTime = bookingTime;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(int createdTime) {
		this.createdTime = createdTime;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
