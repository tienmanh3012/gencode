package com.esb.gencode.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="s_discount")
public class SaleDiscount {
	@Id
    @Column(name="ID")
    private String id;
	@Column(name = "MERCHANT_ID")
    private String merchantId;
	@Column(name = "NAME")
    private String name;
	@Column(name = "PROMOTION_TYPE")
    private int promotionType;
	@Column(name = "PROMOTION_VALUE")
    private int promotionValue;
	@Column(name = "TYPE")
    private int type;
	@Column(name = "START_TIME")
    private int startTime;
	@Column(name = "END_TIME")
    private int endTime;
	@Column(name = "CREATED_TIME")
    private int createdTime;
	@Column(name = "STATUS")
    private int status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}
	public int getPromotionValue() {
		return promotionValue;
	}
	public void setPromotionValue(int promotionValue) {
		this.promotionValue = promotionValue;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getStartTime() {
		return startTime;
	}
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}
	public int getEndTime() {
		return endTime;
	}
	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(int createdTime) {
		this.createdTime = createdTime;
	}
	
}
