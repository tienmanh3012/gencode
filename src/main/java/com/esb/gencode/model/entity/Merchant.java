package com.esb.gencode.model.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Table(name="m_merchant")
public class Merchant implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name="ID")
    private String id;
	@Column(name="SHORT_NAME")
    private String shortName;
	@Column(name="FULL_NAME")
    private String fullName;
	@Column(name="PHONE")
    private String phone;
	@Column(name="EMAIL")
    private String email;
	@Column(name="ADDRESS")
    private String address;
	@Column(name="LA")
    private float la;
	@Column(name="LO")
    private float lo;
	@Column(name="CATEGORY_ID")
    private int categoryId;
	@Column(name="WEBSITE")
    private String website;
	@Column(name="DESCRIPTION")
    private String description;
	@Column(name="TAG")
	private String tag;
	@Column(name="LOGO")
	private String logo;
	@Column(name="TENANT_CODE")
	private String tenantCode;
	@Column(name="CREATED_AT")
    private int createdAt;
    @Column(name="LAST_MODIFIED_AT")
    private int lastModifiedAt;
    @Column(name="CREATED_BY")
    private String createdBy;
    @Column(name="LAST_MODIFIED_BY")
    private String lastModifiedBy;
    @Column(name="STATUS")
    private int status;
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getLa() {
		return la;
	}
	public void setLa(float la) {
		this.la = la;
	}
	public float getLo() {
		return lo;
	}
	public void setLo(float lo) {
		this.lo = lo;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	public int getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(int createdAt) {
		this.createdAt = createdAt;
	}
	public int getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(int lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}