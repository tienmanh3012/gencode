package com.esb.gencode.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="m_customer")
public class Customer {
	@Id
    @Column(name="ID")
    private String id;
	@Column(name = "MERCHANT_ID")
    private String merchantId;
	@Column(name = "NAME")
    private String name;
	@Column(name = "CUSTOMER_CODE")
    private String customerCode;
	@Column(name = "GENDER")
    private int gender;
	@Column(name = "BIRTHDAY")
    private int birthday;
	@Column(name = "PHONE")
    private String phone;
	@Column(name = "EMAIL")
    private String email;
	@Column(name = "FACEBOOK")
    private String facebook;
	@Column(name = "FROM_SOURCE")
    private String fromSource;
	@Column(name = "NOTE")
    private String note;
	@Column(name = "CREATED_TIME")
    private int createdTime;
	@Column(name = "STATUS")
    private int status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getBirthday() {
		return birthday;
	}
	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public String getFromSource() {
		return fromSource;
	}
	public void setFromSource(String fromSource) {
		this.fromSource = fromSource;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(int createdTime) {
		this.createdTime = createdTime;
	}
}
