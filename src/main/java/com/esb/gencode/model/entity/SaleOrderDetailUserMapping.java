package com.esb.gencode.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Table(name="s_order_detail_user_mapping")
public class SaleOrderDetailUserMapping {
	@Id
    @Column(name="ID")
    private String id;
	@Column(name="MERCHANT_ID")
	private String merchantId;
	@Column(name="ORDER_ID")
    private String orderId;
	@Column(name="ORDER_DETAIL_ID")
    private String orderDetailId;
	@Column(name="USER_ID")
    private String userId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
