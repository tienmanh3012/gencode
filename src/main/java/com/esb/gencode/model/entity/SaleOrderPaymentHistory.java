package com.esb.gencode.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="s_order_payment_history")
public class SaleOrderPaymentHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name="ID")
    private String id;
	@Column(name="MERCHANT_ID")
	private String merchantId;
	@Column(name="ORDER_ID")
    private String orderId;
	@Column(name="AMOUNT")
    private int amount;
	@Column(name="REFUND_AMOUNT")
    private int refundAmount;
	@Column(name="PAYMENT_METHOD")
    private int paymentMethod;
	@Column(name="CREATED_TIME")
    private int createdTime;
	@Column(name="STATUS")
    private int status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public int getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(int createdTime) {
		this.createdTime = createdTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(int refundAmount) {
		this.refundAmount = refundAmount;
	}
}
